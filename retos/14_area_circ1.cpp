#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159

void procesar_area(float radio){
	//A = pi · r²
	float area;

	area = PI * (radio * radio);		//tambien se puede sustituir r*r por area = PI * pow(radio, 2);
	printf("Area = %.2f\n\n", area);
}

int main(int argc, char *argv[]){

	float radio;

	printf("Para PARAR pulsa 0 en el valor del radio\n\n\n");

	do{
		printf("Radio = "); scanf("%f", &radio);
		procesar_area(radio);
	}while(radio>0);

	return EXIT_SUCCESS;
}
