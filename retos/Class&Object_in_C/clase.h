#ifndef __CLASE_H__
#define __CLASE_H__

#include <stdio.h>

struct Clase
{
	void (*mtd1)();
	void (*mtd2)();
};

void metodo1();

void metodo2();

void constructorClase(struct Clase *obj1);

#endif