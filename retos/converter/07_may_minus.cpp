#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX 0x60

const char *type_capital[2]={"minusculas-->MAYUSCULAS", "MAYUSCULAS-->minusculas"};

int menu(){
	int list=1;
	int opt;

	system("clear");
	system("toilet -fpagga --gay 'CONVERTIDOR'");
	printf("\n\n\n");
	for(int i=0; i<2; i++){
		printf("\t\t%i. %s\n", list, type_capital[i]);
		list++;
	}

	printf("\n\tOpcion: ");
	scanf("%i", &opt);

	return opt;
}

void preguntar_cadena(char str[MAX]){
	printf("Introduce la cadena: ");
	scanf("%[^\n]", str);
}

char mayus(char str1[MAX]){
	for(int i=0; i<strlen(str1); i++)
		str1[i] = toupper(str1[i]);
	
	return str1[MAX];
}

char minus(char str1[MAX]){
	for(int i=0; i<strlen(str1); i++)
		str1[i] = tolower(str1[i]);

	return str1[MAX];
}

int main(int argc, char *argv[]){

	int optn;
	char cadena[MAX];

	do{
		optn = menu();
	}while(optn!=1 && optn!= 2);

	__fpurge(stdin);
	preguntar_cadena(cadena);

	if(optn==1){
		printf("Tu cadena en mayusculas es: ");
		mayus(cadena);
	}else{
		printf("Tu cadena en minusculas es: ");
		minus(cadena);
	}
	

	printf("%s\n", cadena);

	return EXIT_SUCCESS;
}
