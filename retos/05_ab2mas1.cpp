#include <stdio.h>

void upload(int num){
	int ab=num*num+1;
	printf ("si b=%d, entonces a=%d\n", num, ab);
}


int main(){
	int a, b=0;
	a=b*b+1; 		//formula principal del programa

	printf("\t\ta = b * b + 1\n");
	upload(b); 		//llama a la funcion y envia como parametro el valor de b
	b=2; 			//cambia el valor de b
	upload(b);
	b=-2;
	upload(b);

	return 0;
}
