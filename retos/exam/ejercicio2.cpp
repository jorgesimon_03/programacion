#include <stdio.h>

float factorial(float num){
	if(num==1)
		return (1);
	else
		return (num * factorial(num-1));
}

float mini_factorial(float num){
	float nombre=1;

	for(int i=1; i<num+1; i++)
		nombre += (1/factorial(i));

	return nombre;

}

int main(){

	float x;

	printf("Escribe un numero: ");
	scanf("%f", &x);

	printf("El factorial de %.2f es %.2f\n", x, factorial(x));
	printf("El segundo ejercicio es: %.2f\n", mini_factorial(x));


	return 0;
}