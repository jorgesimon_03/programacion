#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20

int cer(){return (0);}
int uno(){return (1);}

int (*catalogo[])(){ &cer, &uno };

void imprimir_binario(int num){
	const int base=num;
	/*********************************
	 * 	realloc(): invalid next size
	 *	Abortado (`core' generado)
	**********************************/
	/*
	int *binario_provi = (int *)malloc(MAX * sizeof(int *));
	int var, contador=0;
	do{
		var = (*catalogo[num%2])();
		binario_provi = (int *)realloc(binario_provi, sizeof(int));
		binario_provi[contador] = var;
		num /= 2;
		contador++;
	}while(num>0);

	for(int i=0; i<contador; i++)
		printf("%i", *(binario_provi+i));

	printf("\n");

	free(binario_provi);
	*/
	
	int binario_provi[MAX];
	int contador=0;
	do{
		binario_provi[contador] = (*catalogo[num%2])();
		num /= 2;
		contador++;
	}while(num>0);

	printf("El numero %i en binario es: ", base);
	for(int i=contador-1; i>=0; i--){
		if(i==3 || i==7 || i==11)printf(" ");
		printf("%i", binario_provi[i]);
	}


	printf("\n");
}

char hex00(){return '0';}
char hex01(){return '1';}
char hex02(){return '2';}
char hex03(){return '3';}
char hex04(){return '4';}
char hex05(){return '5';}
char hex06(){return '6';}
char hex07(){return '7';}
char hex08(){return '8';}
char hex09(){return '9';}
char hex10(){return 'A';}
char hex11(){return 'B';}
char hex12(){return 'C';}
char hex13(){return 'D';}
char hex14(){return 'E';}
char hex15(){return 'F';}

char (*indice[])(){
	&hex00, &hex01, &hex02, &hex03, &hex04, &hex05, &hex06, &hex07, &hex08, &hex09, &hex10, &hex11, &hex12, &hex13, &hex14, &hex15
};

void imprimir_hexadec(int num){
	int var, contador=0, nombre;
	int hexadecimal[MAX];
	char hexadecimal_dos[MAX];

	printf("El numero %i en hexadecimal es: ", num);

	do{
		var = num%16;
		hexadecimal[contador] = var;
		num /= 16;
		contador++;
	}while(num>16);	hexadecimal[contador] = num; contador++;
	

	for(int i=0; i<contador; i++){
		nombre = hexadecimal[i];
		hexadecimal_dos[i] = (*indice[nombre])();
	}

	for(int j=contador-1; j>=0; j--)
		printf("%c", hexadecimal_dos[j]);

	printf("\n");
}

int main(){

	int x;

	printf("Escribe un numero: ");
	scanf("%i", &x);

	imprimir_binario(x);
	imprimir_hexadec(x);

	return 0;
}