#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX 0x30

int main(int argc, char *argv[]){

	int num, resto;

	printf("Numero: "); scanf("%i", &num);

	printf("Divisores: ");
	for(int i=1; i<num; i++){
		resto = num%i;
		if(!resto)
			printf("%i ", i);
	}

	printf("\n");	

	return EXIT_SUCCESS;
}

