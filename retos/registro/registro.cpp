#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x30
#define DAT 5

const char *info_user[] = {
	"Nombre",
	"Apellido",
	"Edad",
	"Altura",
	"Peso",
	NULL
};

struct DPerson {
	char name[MAX];
	char surname[MAX];
	int age;
	float altura;
	float peso;
};

void menu(){

	system("clear");
	system("toilet -fpagga --metal 'TUS DATOS'");
	printf("\n\n");
}

void nam(struct DPerson *clien){scanf("%s", clien->name);}
void srm(struct DPerson *clien){scanf("%s", clien->surname);}
void edd(struct DPerson *clien){scanf("%i", &clien->age);}
void alt(struct DPerson *clien){scanf("%f", &clien->altura);}
void hei(struct DPerson *clien){scanf("%f", &clien->peso);}
void (*indice[])(struct DPerson *clien) = { &nam, &srm, &edd, &alt, &hei };

void respuesta_pregunta(struct DPerson *cl, int j){

	(*indice[j])(cl);
}

void enunciado_pregunta(int k){

	__fpurge(stdin);
	printf("%i. %s: ", k+1, info_user[k]);
	
	k++;
}


void preguntar_datos(struct DPerson *client){

	for(int i=0; i<DAT; i++){
		enunciado_pregunta(i);
		respuesta_pregunta(client, i);
	}
}

void preguntar_repeticion(char str[MAX]){
	printf("Quieres introducir otro cliente?(s/n) ");
	scanf("%s", str);
}

void imprimir_datos(struct DPerson clien){

	FILE *pf;

	pf = fopen("bbdd.txt", "a");

	fprintf(pf, "%s\t\t||\t%s\t\t||  %i\t||\t%.2f\t||\t%.2fkg\n", clien.name, clien.surname, clien.age, clien.altura, clien.peso);

	fclose(pf);
}

int main(int argc, char *argv[]){

	struct DPerson cliente;

	char otromas[MAX];
	
	menu();
	preguntar_repeticion(otromas);
	while( strcmp(otromas, "n") ){

		menu();
		__fpurge(stdin);
		preguntar_datos(&cliente);
		preguntar_repeticion(otromas);

		imprimir_datos(cliente);
	}
	
	menu();
	system("cat bbdd.txt");



	return EXIT_SUCCESS;
}

