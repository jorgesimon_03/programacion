#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


enum TOpcion {hipotenusa=1, cateto};

float hipo(){
	float cat1, cat2, hipote;
	printf("Medidas en cm.\n");
	printf("Cateto1: "); scanf("%f", &cat1);
	printf("Cateto2: "); scanf("%f", &cat2);
	cat1 *= cat1;
	cat2 *= cat2;
	hipote = sqrt(cat1+cat2);
	printf("La hipotenusa mide: %.2fcm\n", hipote);
	return 0;
}

float catet(){
	float cat1, cat2, hipote;
	printf("Medidas en cm:\n");
	printf("Cateto1: "); scanf("%f", &cat1);
	printf("Hipotenusa: "); scanf("%f", &hipote);
	cat1 *= cat1;
	hipote *= hipote;
	cat2 = sqrt(hipote-cat1);
	printf("El cateto2 mide: %.2fcm\n", cat2);
	return 0;
}

int main(){
	int num;
	printf("Que quieres saber:\n\t[1]=hipotenusa\n\t[2]=cateto\n"); scanf("%d", &num);

	switch(num){
		case hipotenusa:
			hipo();
			break;
		case cateto:
			catet();
			break;
		default:
			printf("Operacion no encontrada\n");
	}



/*	if(num==1)
		hipo();
	else if(num==2)
		catet();
	else
		printf("Operacion no encontrada\n");
*/




	return 0;
}

