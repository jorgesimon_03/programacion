#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

const char letter_symbols[]={"1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!$()=@#"};

int main(int argc, char *argv[]){

	srand (time (NULL));

	int tam_pass;
	int posi;

	printf("Escribe la longitud de la contraseña: "); scanf("%i", &tam_pass);
	for(int i=0; i<tam_pass; i++){
		posi = rand () % strlen(letter_symbols);
		printf("%c", letter_symbols[posi]);
	}
	printf("\n");

	//sleep(5); system("clear");

	return EXIT_SUCCESS;
}
