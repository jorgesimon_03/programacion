#include <iostream>
#include "clases.h"
using namespace std;

Calculator::Calculator(){
	cout << "Inicio" << endl;
}

int Calculator::preguntar_ops(){
	cout << "Operador 1: ";
	cin >> this->op1;
	cout << "Operador 2: ";
	cin >> this->op2;
	return op1;
	
}


void Calculator::suma(){
	cout << this->op1 + this->op2 << endl;
}

void Calculator::rest(){
	cout << this->op1 - this->op2 << endl;
}

void Calculator::mult(){
	cout << this->op1 * this->op2 << endl;
}

void Calculator::divi(){
	cout << this->op1 / this->op2 << endl;
}


Calculator::~Calculator(){
	cout << "Fin" << endl;
}