#include <stdlib.h>
#include <iostream>
using namespace std;

#include "clases.h"
#include "algorims.h"


void (Calculator::*catalogo[])() = {
	&Calculator::suma,
	&Calculator::rest,
	&Calculator::mult,
	&Calculator::divi,
	
};


int main(int argc, char *argv[]){
	
	Calculator micalc;

	
	int operador;

	title();

	menu(&operador);


	micalc.preguntar_ops();

	(micalc.*catalogo[operador])();
	/*switch(operador){
		case 1:
			micalc.suma();
			break;
		case 2:
			micalc.rest();
			break;
		case 3:
			micalc.mult();
			break;
		case 4:
			micalc.divi();
			break;
		case 5:
			return EXIT_SUCCESS;
		default:
			cout << "Error\n";
	}*/
	
	return EXIT_SUCCESS;
}
