#ifndef __CLASES_H_
#define __CLASES_H_

class Calculator{


public:
	//Constructor
	Calculator();

	int preguntar_ops();

	void suma();
	void rest();
	void mult();
	void divi();

	//Destructor
	~Calculator();
protected:

private:
	int op1, op2;
	
};

#endif