#include <stdio.h>
#include <stdlib.h>

#include "kestion.h"
#include "movement.h"

#define MAX 3
#define PER 5

int (*indice[])() = { &preguntar_num_uno, &preguntar_num_dos, &preguntar_num_tre };
void (*libro[][PER])(int cubo[MAX][MAX][MAX],  int x) = {
	{ &girar_col_cero, &girar_col_noventa, &girar_col_cientoochenta, &girar_col_doscientosetenta, &girar_col_cero },
	{ &girar_fil_cero, &girar_fil_noventa, &girar_fil_cientoochenta, &girar_fil_doscientosetenta, &girar_fil_cero },
	{ &girar_prf_cero, &girar_prf_noventa, &girar_prf_cientoochenta, &girar_prf_doscientosetenta, &girar_prf_cero }
};



int main(int argc, char *argv[]){

	int cubito[MAX][MAX][MAX];
	int op1, op2, op3, op4;

	int num=1;
	for(int x=0; x<MAX; x++){for(int y=0; y<MAX; y++){for(int z=0; z<MAX; z++){
				cubito[x][y][z] = num;
				num++;
	}}}

	imprimir(cubito);

		
	op1 = preguntar_eje();		//col, fil, prof
	op2 = (*indice[op1])();		//izq, center, der
	op3 = preguntar_deg();		//grados


	(*libro[op1][op3])(cubito, op2);


	return EXIT_SUCCESS;
}