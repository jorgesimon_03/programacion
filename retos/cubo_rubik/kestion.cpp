#include <stdio.h>
#include <stdlib.h>

#include "kestion.h"

void clean(){
	system("clear");
}

int preguntar_eje(){

	int op;

	do{
		clean();

		printf("Que eje quieres mover??\n\n");

		printf("[1] Columna\n");
		printf("[2] Fila\n");
		printf("[3] Profundidad\n");

		printf("\t\t::"); scanf("%i", &op);
	}while( op<1 || op>3 );

	return --op;
}


int preguntar_num_uno(){

	int op;

	do{

		clean();

		printf("Que columna quires mover??\n\n");

		printf("[1] La de la izquierda\n");
		printf("[2] La del medio\n");
		printf("[3] La de la derecha\n");

		printf("\t\t::"); scanf("%i", &op);
	}while( op<1 || op>3 );

	return --op;
}

int preguntar_num_dos(){

	int op;

	do{

		clean();

		printf("Que fila quires mover??\n\n");

		printf("[1] La de arriba\n");
		printf("[2] La del medio\n");
		printf("[3] La de abajo\n");

		printf("\t\t::"); scanf("%i", &op);
	}while( op<1 || op>3 );

	return --op;
}

int preguntar_num_tre(){

	int op;

	do{

		clean();

		printf("Que \"Profundidad\" quires mover??\n\n");
	
		printf("[1] La de atras\n");
		printf("[2] La del medio\n");
		printf("[3] La de delante\n");

		printf("\t\t::"); scanf("%i", &op);
	}while( op<1 || op>3 );

	return --op;
}

int preguntar_deg(){

	int op;

	do{

		clean();

		printf("Cuantos grados los quieres moverlo??\n\n");
	
		printf("[1] 0   grados\n");
		printf("[2] 90  grados \n");
		printf("[3] 180 grados\n");
		printf("[4] 270 grados\n");
		printf("[5] 360 grados\n");

		printf("\t\t::"); scanf("%i", &op);
	}while( op<1 || op>5 );

	return --op;
}