#ifndef __MOVEMENT_H__
#define __MOVEMENT_H__

#define MAX 3

void imprimir					(int cubo[MAX][MAX][MAX]);

void girar_col_cero				(int cubo[MAX][MAX][MAX],  int colum);
void girar_col_noventa			(int cubo[MAX][MAX][MAX],  int colum);
void girar_col_cientoochenta	(int cubo[MAX][MAX][MAX],  int colum);
void girar_col_doscientosetenta	(int cubo[MAX][MAX][MAX],  int colum);

void girar_fil_cero				(int cubo[MAX][MAX][MAX],  int fila);
void girar_fil_noventa			(int cubo[MAX][MAX][MAX],  int fila);
void girar_fil_cientoochenta	(int cubo[MAX][MAX][MAX],  int fila);
void girar_fil_doscientosetenta	(int cubo[MAX][MAX][MAX],  int fila);

void girar_prf_cero				(int cubo[MAX][MAX][MAX],  int profu);
void girar_prf_noventa			(int cubo[MAX][MAX][MAX],  int profu);
void girar_prf_cientoochenta	(int cubo[MAX][MAX][MAX],  int profu);
void girar_prf_doscientosetenta	(int cubo[MAX][MAX][MAX],  int profu);


#endif