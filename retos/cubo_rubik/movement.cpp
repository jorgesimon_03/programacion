#include <stdio.h>
#include <stdlib.h>

#include "movement.h"

void imprimir(int cubo[MAX][MAX][MAX]){
	for(int x=0; x<MAX; x++){
		for(int y=0; y<MAX; y++){
			for(int z=0; z<MAX; z++){
				printf("%i ", cubo[x][y][z]);
			}
			printf("\n");
		}
		printf("\n");
	}
}




/****************************************
 *          MOVER LAS COLUMNAS          *
 ***************************************/
void girar_col_cero(int cubo[MAX][MAX][MAX],  int colum){

	printf("\n\nGiro de 0 o 360 grados:\n");
	imprimir(cubo);
}

void girar_col_noventa(int cubo[MAX][MAX][MAX], int colum){

	int lista[9], num=0, num1 = 6;

	for(int x=0; x<MAX; x++)
		for(int y=0; y<MAX; y++){
			lista[num] = cubo[x][y][colum];
			num++;
		}

	for(int x=0; x<MAX; x++){
		for(int y=0; y<MAX; y++){
			cubo[x][y][colum] = lista[num1];
			num1-=3;
		}
		num1+=10;
	}

	printf("\n\nGiro 90: \n");	
	imprimir(cubo);
	
}

void girar_col_cientoochenta(int cubo[MAX][MAX][MAX], int colum){

	int lista[9], num=0, num2=8;

	for(int x=0; x<MAX; x++)
		for(int y=0; y<MAX; y++){
			lista[num] = cubo[x][y][colum];
			num++;
		}

	for(int x=0; x<MAX; x++){
		for(int y=0; y<MAX; y++){
			cubo[x][y][colum] = lista[num2];
			num2-=1;
		}
	}

	printf("\n\nGiro 180: \n");
	imprimir(cubo);
	
}
void girar_col_doscientosetenta	(int cubo[MAX][MAX][MAX], int colum){

	int lista[9], num=0, num3=2;

	for(int x=0; x<MAX; x++)
		for(int y=0; y<MAX; y++){
			lista[num] = cubo[x][y][colum];
			num++;
		}

	for(int x=0; x<MAX; x++){
		for(int y=0; y<MAX; y++){
			cubo[x][y][colum] = lista[num3];
			num3+=3;
		}
		num3-=10;
	}

	printf("\n\nGiro 180: \n");
	imprimir(cubo);

}




/****************************************
 *          MOVER LAS FILAS             *
 ***************************************/
void girar_fil_cero(int cubo[MAX][MAX][MAX],  int fila){

	imprimir(cubo);

}

void girar_fil_noventa(int cubo[MAX][MAX][MAX], int fila){
	
	int lista[9], i=0, num4=2;

	for(int y=0; y<MAX; y++)
		for(int z=0; z<MAX; z++){
			lista[i] = cubo[fila][y][z];
			i++;
		}

	for(int y=0; y<MAX; y++){
		for(int z=0; z<MAX; z++){
			cubo[fila][y][z] = lista[num4];
			num4+=3;
		}
		num4-=10;
	}

	printf("\n\nGiro 90: \n");
	imprimir(cubo);
}

void girar_fil_cientoochenta(int cubo[MAX][MAX][MAX], int fila){
	
	int lista[9], i=0, num5=8;

	for(int y=0; y<MAX; y++)
		for(int z=0; z<MAX; z++){
			lista[i] = cubo[fila][y][z];
			i++;
		}

	for(int y=0; y<MAX; y++){
		for(int z=0; z<MAX; z++){
			cubo[fila][y][z] = lista[num5];
			num5-=1;
		}
	}

	printf("\n\nGiro 180: \n");
	imprimir(cubo);

}

void girar_fil_doscientosetenta	(int cubo[MAX][MAX][MAX],  int fila){

	int lista[9], i=0, num6=6;

	for(int y=0; y<MAX; y++)
		for(int z=0; z<MAX; z++){
			lista[i] = cubo[fila][y][z];
			i++;
		}

	for(int y=0; y<MAX; y++){
		for(int z=0; z<MAX; z++){
			cubo[fila][y][z] = lista[num6];
			num6-=3;
		}
		num6+=10;
	}

	printf("\n\nGiro 270: \n");
	imprimir(cubo);

}




/****************************************
 *          MOVER LA PROFUNDIDAD        *
 ***************************************/
void girar_prf_cero(int cubo[MAX][MAX][MAX],  int profu){

	printf("\n\nGiro 0 o 360 grados\n");
	imprimir(cubo);

}

void girar_prf_noventa(int cubo[MAX][MAX][MAX],  int profu){

	int lista[9], i=0, num7=6;

	for(int x=0; x<MAX; x++)
		for (int z=0; z<MAX; z++){
			lista[i] = cubo[x][profu][z];
			i++;
		}

	for(int x=0; x<MAX; x++){
		for(int z=0; z<MAX; z++){
			cubo[x][profu][z] = lista[num7];
			num7-=3;
		}
		num7+=10;
	}

	printf("\n\nGiro 90: \n");
	imprimir(cubo);

}

void girar_prf_cientoochenta	(int cubo[MAX][MAX][MAX],  int profu){

	int lista[9], i=0, num8=8;

	for(int x=0; x<MAX; x++)
		for (int z=0; z<MAX; z++){
			lista[i] = cubo[x][profu][z];
			i++;
		}

	for(int x=0; x<MAX; x++)
		for(int z=0; z<MAX; z++){
			cubo[x][profu][z] = lista[num8];
			num8-=1;
		}

	printf("\n\nGiro 180: \n");
	imprimir(cubo);

}

void girar_prf_doscientosetenta	(int cubo[MAX][MAX][MAX],  int profu){

	int lista[9], i=0, num9=2;

	for(int x=0; x<MAX; x++)
		for(int z=0; z<MAX; z++){
			lista[i] = cubo[x][profu][z];
			i++;
		}

	for(int x=0; x<MAX; x++){
		for(int z=0; z<MAX; z++){
			cubo[x][profu][z] = lista[num9];
			num9+=3;
		}
		num9-=10;
	}

	printf("\n\nGiro 270: \n");
	imprimir(cubo);

}
