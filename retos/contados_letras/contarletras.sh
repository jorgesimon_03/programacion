#!/bin/bash

conjcarac=abcdefghijklmnñopqrtsuvwxyz
size1=${#conjcarac}

sed -i 's/ //g' fichero
strinput=$(cat fichero)
size2=${#strinput}

num=0
for((i=0; i<$size1; i++))
do
	count=0
	for((j=0; j<$size2; j++))
	do
		if [ ${conjcarac:$i:1} = ${strinput:$j:1} ]; then
			let count=$count+1
		fi
	done

	if [ $count -ne $num ]; then
		echo "hay $count veces la letra ${conjcarac:$i:1}"
	fi
done

rm -r fichero
