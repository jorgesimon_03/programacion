#include "body.h"
#include "process.h"

const char *str_menuPs[] = {
	"Procesos activos del sistema",
	"Procesos del usuario actual"
};

void (*procesos[])() = {
	&processActive,
	&processUser
};


void contInfinito(){
	for (int i=1; ;i++){
		printf("Numero %i\n", i);
	}
}

void menuPs(){
	int opcion=0;
	printf("\tMENU PROCESOS\n");
	for (int i=1; i<=(sizeof(str_menuPs) / sizeof(char *)); i++){
		printf("\t%i. - %s\n", i, str_menuPs[i-1]);
	}

	printf("\tOpcion:: ");
	scanf("%i", &opcion);

	if(opcion>0 && opcion<3){
		(*procesos[opcion-1])();
	}else{
		printf("Valor no admitido");
	}
}

void salir(){printf("Cerrando el programa\n");exit(0);}
