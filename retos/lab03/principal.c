#include <stdio.h>
#include "menu.h"
#include "body.h"

void (*menuIndice[])() = { 
	&contInfinito,
	&menuPs,
	&salir
};


int main(int argc, char const *argv[])
{
	int opPrincipal = menuPrincipal();
	
	if (opPrincipal>0 && opPrincipal<6){
		(*menuIndice[opPrincipal-1])();
	}else{
		printf("Valor no admitido\n");
		return 0;
	}

	return 0;
}