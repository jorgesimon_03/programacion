#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RESET "\033[0m"

void textoinclor(){
	srand (time (NULL));

	//Elijo un color
        const char *colores [] = {
                "\x1B[31m",
                "\x1B[32m",
                "\x1B[33m",
                "\x1B[34m",
                "\x1B[35m",
                "\x1B[36m"
        };
        long long int num_colores = sizeof (colores) / sizeof (const char *);
        int indice = rand () % num_colores;

	//Elijo un colour_name
	const char *namecolor [] = {
		"Rojo",
		"Azul",
		"Naranja",
		"Verde",
		"Verde oscuro",
		"Blanco"
	};
	long long int numero_colors = sizeof (namecolor) / sizeof (const char *);
	int indice_nam_color = rand () % numero_colors;

	//Imprimo el colour_name en el color que he elegido
        printf ("%s%s%s\n", colores[indice], namecolor[indice_nam_color], RESET);
}

int main (int argc, char *argv[]) {
	textoinclor();

        return EXIT_SUCCESS;
}

