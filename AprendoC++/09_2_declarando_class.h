#ifndef __09_2_DECLARANDO_CLASS_H_
#define __09_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Estatico{
public:
	Estatico(){
		contador++;
	}

	int getContador();
	static int sumar(int num1, int num2);
	
	~Estatico(){}

private:
	static int contador;
};

#endif