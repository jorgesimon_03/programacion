/*****************
  MIEMBROS STATIC
*****************/
#include <iostream>
#include "09_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Estatico *obj1 = new Estatico();
	Estatico *obj2 = new Estatico();

	cout << obj1->getContador() << endl;

	Estatico *obj3 = new Estatico();

	cout << "la suma es: " << Estatico::sumar(4, 6) << endl;

	cout << obj1->getContador() << endl;
	

	return 0;
}