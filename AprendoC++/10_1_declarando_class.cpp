/******************
  FUNCIONES AMIGAS  
******************/
#include <iostream>
#include "10_2_declarando_class.h"
using namespace std;

void modificar(Personaje &p, int atq, int def){
	p.ataque = atq;
	p.defensa = def;
}

int main(int argc, char *argv[]){

	Personaje *nuevo = new Personaje(100, 90);

	nuevo->mostrarDatos();

	modificar(*nuevo, 12, 56);

	nuevo->mostrarDatos();	

	return 0;
}