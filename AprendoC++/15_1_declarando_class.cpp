/******************
  HERENCIA PRIVADA
******************/
#include <iostream>
#include "15_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	string marca, modelo, color;
	int cilin;
	Deportivo *fyf;
	cout << "Marca: "; cin >> marca;
	cout << "Modelo: "; cin >> modelo;
	cout << "Cilindrada: "; cin >> cilin;

	fyf = new Deportivo(marca, color, modelo, cilin);

	fyf->todos();
	cout << "Nºcilin: " << fyf->obtenerCilindrada() << endl;


	delete fyf;
	return 0;
}