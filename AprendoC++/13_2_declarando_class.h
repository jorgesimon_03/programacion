#ifndef __13_2_DECLARANDO_CLASS_H_
#define __13_2_DECLARANDO_CLASS_H_

#include <iostream>
#include <math.h>
using namespace std;

class Figura{
public:
	Figura(int nLados){
		this->nLados = nLados;
	}

	int getNLados();

	~Figura(){	}
	
private:
	int nLados;
};

class Triangulo: public Figura
{
public:
	Triangulo(int nLados, float lado1, float lado2, float lado3): Figura(nLados)
	{
		this->lado1 = lado1;
		this->lado2 = lado2;
		this->lado3 = lado3;
	}

	float areaTriangulo();

	~Triangulo(){}

private:
	float lado1, lado2, lado3;;
	
};


#endif