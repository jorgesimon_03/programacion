/*************
  EJERCICIO 3
*************/
#include <iostream>
#include "12_2_declarando_class.h"
using namespace std;

#define PER 3

int main(int argc, char *argv[]){

	int x, y;
	cout << "Donde quires empezar?? " << endl;
	cout << "Eje X: ";
	cin >> x;
	cout << "Eje Y: ";
	cin >> y;

	Posicion *coordenadas = new Posicion(x, y);

	for(;;)
		coordenadas->mover();


	return 0;
}