/****************
  CLASE DERIVADA
****************/
#include <iostream>
#include "13_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	float l1, l2, l3;
	Triangulo *t1;
	cout << "lado1?? ";
	cin >> l1;
	cout << "lado2?? ";
	cin >> l2;
	cout << "lado3?? ";
	cin >> l3;

	t1 = new Triangulo(3, l1, l2, l3);

	cout << "Numero de lados " << t1->getNLados() << endl;
	cout << "area total: " << t1->areaTriangulo() << endl;

	return 0;
}
