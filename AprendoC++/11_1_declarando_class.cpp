/*************
  EJERCICIO 2
*************/
#include <iostream>
#include "11_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Cuadrilatero *obj1;
	float x, y;

	cout << "Escribe un lado: ";
	cin >> x;
	cout << "Escribe otro lado: ";
	cin >> y;

	if(x==y)
		obj1 = new Cuadrilatero(x);
	else
		obj1 = new Cuadrilatero(x, y);
	
	obj1->mostrarArea();
	obj1->mostrarPeri();
	

	return 0;
}
