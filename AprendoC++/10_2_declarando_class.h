#ifndef __10_2_DECLARANDO_CLASS_H_
#define __10_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Personaje{
	
	friend void modificar(Personaje &p, int atq, int def);

public:
	Personaje(int ataque, int defensa){
		this->ataque = ataque;
		this->defensa = defensa;
	}

	void mostrarDatos();


	~Personaje(){}

private:
	int ataque, defensa;
	
};

#endif