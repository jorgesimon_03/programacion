#ifndef __08_2_DECLARANDO_CLASS_H_
#define __08_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Expediente
{
public:
	Expediente(int numExp){
		this->numExp = numExp;
	}
	Expediente(){}

	int getNumExp();


	~Expediente(){}
	
private:
	int numExp;

};


class Direccion
{
public:
	Direccion(string direccion){
		this->direccion = direccion;
	}
	Direccion(){}

	string getDireccion();

	~Direccion(){}

private:
	string direccion;
};

class Estudiante
{
public:
	Estudiante(string codigo, float media, int numExp, string direccion): exp(numExp), dir(direccion)
	{
		this->codigo = codigo;
		this->media = media;
	}

	void mostrarDatos();

	~Estudiante(){}
	
private:
	string codigo;
	float media;

	Expediente exp;
	Direccion dir;
};


#endif