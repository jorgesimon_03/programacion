#ifndef __02_2_DECLARANDO_CLASS_H_
#define __02_2_DECLARANDO_CLASS_H_

class Punto{
public:
	Punto(int _x, int _y){
		x = _x;
		y = _y;
	}

	Punto(){
		x = y = 0;
	}

	void setX(int valorX);
	void setY(int valorY);
	int getX();
	int getY();

	~Punto(){}

private:
	int x, y;
	
};

#endif