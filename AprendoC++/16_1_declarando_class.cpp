/*****************
  CLASE ABSTRACTA
*****************/
#include <iostream>
#include "16_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Planta *p1 = new Planta;
	AnimalCarnivoro *ac1 = new AnimalCarnivoro;
	AnimalHerbivoro *ah1 = new AnimalHerbivoro;
	
	p1->alimentarse();
	ac1->alimentarse();
	ah1->alimentarse();

	return 0;
}
