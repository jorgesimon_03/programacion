#include <iostream>
#include "12_2_declarando_class.h"
using namespace std;

void Posicion::mostrar(){
	cout << this->x << " -y- " << this->y << endl;
}

void Posicion::mover(){
	int opt;
	cout << "Hacia donde quieres ir: " << endl;
	cout << "[1]Izq\n[2]Der\n[3]Abajo\n[4]Arriba\n";
	do{
		cin >> opt;
	}while(opt<1 || 4<opt);

	switch(opt){
		case 1:
			this->x-=5;
			break;
		case 2:
			this->x+=5;
			break;
		case 3:
			this->y-=5;
			break;
		case 4:
			this->y+=5;
			break;
		default: 
			cout << "Eres un genio\n";
	}

	mostrar();
}
