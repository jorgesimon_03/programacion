/*****************
  CLASE COMPUESTA
*****************/
#include <iostream>
#include "08_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	//Estudiante jorge("JS2022", 7.64, 1234567, "C/Gran Via");
	Estudiante *jorge = new Estudiante("JS2022", 7.64, 1234567, "C/Gran Via");

	//jorge.mostrarDatos();
	jorge->mostrarDatos();
	

	return 0;
}