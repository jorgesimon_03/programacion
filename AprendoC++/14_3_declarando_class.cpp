#include <iostream>
#include "14_2_declarando_class.h"
using namespace std;


string Vehiculo::getMarca(){
	return marca;
}

string Vehiculo::getColor(){
	return color;
}

string Vehiculo::getModelo(){
	return modelo;
}


string Turismo::getRetModelo(){
	return getModelo();
}

int Turismo::getNumPuertas(){
	return numPuertas;
}
