#ifndef __06_2_DECLARANDO_CLASS_H_
#define __06_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Perro{
public:
	Perro(){
		cout << "Constructor\n";
	}



	~Perro(){
		cout << "Destructor\n";
	}
	
};


#endif
