#ifndef __07_2_DECLARANDO_CLASS_H_
#define __07_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Coche{
public:
	Coche(string marca, int km);

	void mostrarCoche();

	~Coche(){	}
	
private:
	string marca;
	int km;
};


#endif