#ifndef __16_2_DECLARANDO_CLASS_H_
#define __16_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class SerVivo
{
public:
	SerVivo(){}

	virtual void alimentarse() = 0;

	~SerVivo(){}
	
};

class Planta: public SerVivo
{
public:
	Planta(){}

	void alimentarse();

	~Planta(){}
	
};

class Animal: public SerVivo
{
public:
	Animal(){}

	virtual void alimentarse() = 0;

	~Animal(){}
	
};

class AnimalCarnivoro: public Animal
{
public:
	AnimalCarnivoro(){}

	void alimentarse();

	~AnimalCarnivoro(){}
	
};


class AnimalHerbivoro: public Animal
{
public:
	AnimalHerbivoro(){}

	void alimentarse();

	~AnimalHerbivoro(){}
	
};



#endif