#ifndef __03_2_DECLARANDO_CLASS_H_
#define __03_2_DECLARANDO_CLASS_H_

class DiaAnio{
public:
	DiaAnio(int _dia, int _mes){
		this->dia = _dia;
		this->mes = _mes;
	}

	bool igual(DiaAnio &d);
	void visualizar();
	

	~DiaAnio(){	}

private:
	int mes, dia;
};

#endif
