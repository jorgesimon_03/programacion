/****************** 
  ARRAY DE OBJETOS
******************/
#include <iostream>
#include "05_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Alumno buenos[4];

	Alumno *malos = new Alumno[3];

	for(int i=0; i<3; i++)
		(malos+i)->pedirDatos();
	
	cout << "\n\nMostrando datos: " << endl;
	for(int i=0; i<3; i++)
		(malos+i)->mostrarNotas();

	return 0;
}