#ifndef __15_2_DECLARANDO_CLASS_H_
#define __15_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Vehiculo
{
public:
	Vehiculo(string marca, string color, string modelo){
		this->marca = marca;
		this->color = color;
		this->modelo = modelo;
	}

	string getMarca();
	string getColor();

	~Vehiculo(){}

protected:
	string modelo;
	string getModelo();

private:
	string marca, color;
};


class Deportivo: private Vehiculo
{
public:
	Deportivo(string marca, string color, string modelo, int cilindrada): Vehiculo(marca, color, modelo)
	{
		this->cilindrada = cilindrada;
	}

	void todos();

	int obtenerCilindrada();

	~Deportivo(){}

protected:
private:
	int cilindrada;
};
#endif