#include <iostream>
#include "08_2_declarando_class.h"
using namespace std;

int Expediente::getNumExp(){
	return numExp;
}

string Direccion::getDireccion(){
	return direccion;
}

void Estudiante::mostrarDatos(){
	cout << "Codigo: " << codigo << endl;
	cout << "Promedio: " << media << endl;

	cout << "Numero de Expediente: " << exp.getNumExp() << endl;
	cout << "Direccion: " << dir.getDireccion() << endl;
}
