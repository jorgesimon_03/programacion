#include <iostream>
#include "02_2_declarando_class.h"

void Punto::setX(int valorX){
	this->x = valorX;
}

void Punto::setY(int valorY){
	this->y = valorY;
}

int Punto::getX(){
	return this->x;
}

int Punto::getY(){
	return this->y;
}
