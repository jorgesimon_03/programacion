#include <iostream>
#include "05_2_declarando_class.h"
using namespace std;


void Alumno::pedirDatos(){
	cout << "Nota de Bases: ";
	cin >> notaBases;

	cout << "Nota de Programacion: ";
	cin >> notaProgr;
}

void Alumno::mostrarNotas(){
	cout << "Nota de Bases: " << notaBases << endl;
	cout << "Nota de Programacion: " << notaProgr << endl;
	cout << "Nota media: " << (notaBases+notaProgr)/2 << endl;
}
