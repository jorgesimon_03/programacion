#include <iostream>
#include "16_2_declarando_class.h"
using namespace std;


void Planta::alimentarse(){
		cout << "La planta se alimenta mediante la fotosintesis\n";
}

void AnimalCarnivoro::alimentarse(){
		cout << "El animal carnivoro se alimenta mediante de carne\n";
}

void AnimalHerbivoro::alimentarse(){
		cout << "La animal herbivoro se alimenta mediante de hierbas\n";
}