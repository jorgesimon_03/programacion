/*
  EJERCICIO 1
			 */
#include <iostream>
#include "03_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){
	
	DiaAnio *micumple;
	DiaAnio *fechhoy;
	int dia, mes;

	cout << "Que dia es tu cumple?? ";
	cin >> dia;
	cout << "Que mes es tu cumple?? ";
	cin >> mes;
	micumple = new DiaAnio(dia, mes);

	cout << "Que dia es hoy?? ";
	cin >> dia;
	cout << "Que mes es este?? ";
	cin >> mes;
	fechhoy = new DiaAnio(dia, mes);

	if( fechhoy->igual(*micumple) )
		cout << "Felicidades!!\n";
	else
		cout << "Lo sineto, sera otro dia\n";


	delete micumple;
	delete fechhoy;

	return 0;
}
