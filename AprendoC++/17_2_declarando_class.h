#ifndef __17_2_DECLARANDO_CLASS_H_
#define __17_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Poligono
{
public:
	Poligono(){}

	virtual float perimetro() = 0;
	virtual float area() = 0;

	~Poligono(){}
	
};

class Rectangulo: public Poligono
{
public:
	Rectangulo(float base, float altura){
		this->base = base;
		this->altura = altura;
	}
	
	float perimetro();
	float area();

	~Rectangulo(){}

private:
	float base, altura;
};

class Triangulo: public Poligono
{
public:
	Triangulo(float lado1, float lado2, float lado3){
		this->lado1 = lado1;
		this->lado2 = lado2;
		this->lado3 = lado3;
	}

	float perimetro();
	float area();

	~Triangulo();

private:
	float lado1, lado2, lado3;
	
};

#endif