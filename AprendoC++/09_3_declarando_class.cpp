#include <iostream>
#include "09_2_declarando_class.h"
using namespace std;

int Estatico::contador=0;

int Estatico::getContador(){
	return contador;
}

int Estatico::sumar(int num1, int num2){
	return num1+num2;
}
