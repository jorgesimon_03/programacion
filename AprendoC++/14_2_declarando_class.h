#ifndef __14_2_DECLARANDO_CLASS_H_
#define __14_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Vehiculo
{
public:
	Vehiculo(string marca, string color, string modelo){
		this->marca = marca;
		this->color = color;
		this->modelo = modelo;
	}

	string getMarca();
	string getColor();

	~Vehiculo(){}

protected:
	string modelo;
	string getModelo();

private:
	string marca, color;
};


class Turismo: public Vehiculo
{
public:
	Turismo(string marca, string color, string modelo,int numPuertas): Vehiculo(marca, color, modelo)
	{
		this->numPuertas = numPuertas;
	}

	string getRetModelo();

	int getNumPuertas();

	~Turismo(){}

private:
	int numPuertas;
};


#endif