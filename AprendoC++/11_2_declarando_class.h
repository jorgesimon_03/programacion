#ifndef __11_2_DECLARANDO_CLASS_H_
#define __11_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Cuadrilatero{
public:
	Cuadrilatero(float x, float y){
		this->x = x;
		this->y = y;
	}

	Cuadrilatero(float x){
		this->x = this->y = x;
	}

	void mostrarArea();
	void mostrarPeri();


	~Cuadrilatero(){	}

private:
	float x, y;
};

#endif
