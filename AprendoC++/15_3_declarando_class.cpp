#include <iostream>
#include "15_2_declarando_class.h"
using namespace std;

string Vehiculo::getMarca(){
	return marca;
}

string Vehiculo::getColor(){
	return color;
}

string Vehiculo::getModelo(){
	return modelo;
}

int Deportivo::obtenerCilindrada(){
	return cilindrada;
}

void Deportivo::todos(){
	cout << "Marca: " << getMarca() << endl;
	cout << "Modelo: " << getModelo() << endl;
}
