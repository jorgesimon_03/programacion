#include <iostream>
#include <math.h>
#include "17_2_declarando_class.h"
using namespace std;

float Rectangulo::perimetro(){
	return (2*base+2*altura);
}

float Rectangulo::area(){
	return (base*altura);
}

float Triangulo::perimetro(){
	return (lado1+lado2+lado3);
}

float Triangulo::area(){
	float p = (lado1+lado2+lado3)/2;
	float a = sqrt(p*(p-lado1)*(p-lado2)*(p-lado3));
	return a;
}
