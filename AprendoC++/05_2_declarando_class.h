#ifndef __05_2_DECLARANDO_CLASS_H_
#define __05_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Alumno{
public:
	Alumno(){	}

	void pedirDatos();
	void mostrarNotas();

	~Alumno(){	}

private:
	float notaBases, notaProgr, media;

};


#endif