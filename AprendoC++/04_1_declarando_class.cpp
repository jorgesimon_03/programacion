/************************
* SOBRECARGA DE METODOS *
************************/
#include <iostream>
#include "04_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Persona *persona1 = new Persona("jorge", 18);
	persona1->correr();

	Persona *persona2 = new Persona("5990582W");
	persona2->correr(123);
	return 0;
}
