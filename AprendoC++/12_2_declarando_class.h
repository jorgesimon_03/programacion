#ifndef __12_2_DECLARANDO_CLASS_H_
#define __12_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

#define PER 3

class Posicion{
public:
	Posicion(int x, int y){
		this->x = x;
		this->y = y;
	}

	void mover();
	void mostrar();

	~Posicion(){}

private:
	int x, y;
	
};

#endif