/**************
  POLIMORFISMO
**************/
#include <iostream>
#include "17_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Poligono *ejemp[2];

	float bas, alt, lado1, lado2, lado3;
	cout << "Base: "; cin >> bas;
	cout << "ALtura: "; cin >> alt;
	ejemp[0] = new Rectangulo(bas, alt);

	cout << "Lado 1: "; cin >> lado1;
	cout << "Lado 2: "; cin >> lado2;
	cout << "Lado 3: "; cin >> lado3;
	ejemp[1] = new Triangulo(lado1, lado2, lado3);

	cout << endl << endl;
	
	for(int i=0; i<2; i++){
		cout << "PErimetro:: " << ejemp[i]->perimetro() << endl;
		cout << "ARea::      " << ejemp[i]->area() << endl;
	}

	return 0;
}