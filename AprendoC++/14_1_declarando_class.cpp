/******************
  HERENCIA PUBLICA
******************/
#include <iostream>
#include "14_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){

	Turismo *vhc;
	string marca, modelo, color;
	int numPuerta;
	cout << "Marca: "; cin >> marca;
	cout << "Modelo: "; cin >> modelo;
	cout << "Color: "; cin >> color;
	cout << "num pUertas: "; cin >> numPuerta;

	vhc = new Turismo(marca, modelo, color, numPuerta);

	cout << "Marca " << vhc->getMarca() << endl;
	cout << "Modelo " << vhc->getRetModelo() << endl;
	cout << "Color " << vhc->getColor() << endl;
	cout << "Nºpuertas: " << vhc->getNumPuertas() << endl;

	return 0;
}