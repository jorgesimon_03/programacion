#include <iostream>
#include "07_2_declarando_class.h"
using namespace std;

Coche::Coche(string marca, int km){
	this->marca = marca;
	this->km = km;
}

void Coche::mostrarCoche(){
	cout << "MArca: " << marca << " y km: " << km << endl;
}