#include <iostream>
#include "13_2_declarando_class.h"
using namespace std;

int Figura::getNLados(){
	return nLados;
}

float Triangulo::areaTriangulo(){
	float p = (lado1+lado2+lado3)/2;
	float a = sqrt(p*(p-lado1)*(p-lado2)*(p-lado3));
	return a;
}