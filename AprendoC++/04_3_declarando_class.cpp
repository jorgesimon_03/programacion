#include <iostream>
#include "04_2_declarando_class.h"
using namespace std;


void Persona::correr(){
	cout << "soy " << nombre << ", tengo " << edad << " años\n";
}

void Persona::correr(int km){
	cout << "he corrido " << km << " kilometros\n";
}