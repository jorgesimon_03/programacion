#include <iostream>
#include "02_2_declarando_class.h"
using namespace std;

int main(int argc, char *argv[]){
	
	/***Estatica***/
	Punto p1(2, 1); //estaticos

	cout << "Ell valor de xEs: " << p1.getX() << endl;
	cout << "Ell valor de yEs: " << p1.getY() << endl;


	/***Dinamica***/
	Punto *p2 = new Punto();

	p2->setX(5);
	p2->setY(8);

	cout << "El valor de x es: " << p2->getX() << endl;
	cout << "El valor de y es: " << p2->getY() << endl;

	return 0;
}