#ifndef __04_2_DECLARANDO_CLASS_H_
#define __04_2_DECLARANDO_CLASS_H_

#include <iostream>
using namespace std;

class Persona{
public:
	//Sobrecarga de metodos
	Persona(string _nombre, int _edad){
		nombre = _nombre;
		edad = _edad;
	}

	Persona(string _dni){
		dni = _dni;
	}

	void correr();
	void correr(int km);



	~Persona(){	}
	
private:
	string nombre;
	int edad;
	string dni;
};


#endif
