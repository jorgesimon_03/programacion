#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define BLUE_T "\x1b[34m"
#define GREEN_T "\x1b[32m"

int main(){
	int op1;
	int op2;
	int result;
	
	printf ("Escribe operando1: "); scanf ("%d", &op1);
	printf ("Escribe operando2: "); scanf ("%d", &op2);
	result = op1+op2;

	printf (BLUE_T "<%d> + <%d>" " =" GREEN_T  " <%d>\n", op1, op2, result);

	return 0;
}
