#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x50

int main(){
	char arr[MAX];

	fgets(arr, MAX, stdin);
	int ultima = strlen(arr);
	arr[ultima-1] = '\0';
	puts(arr);

	return 0;
}
