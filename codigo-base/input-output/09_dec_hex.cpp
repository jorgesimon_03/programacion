#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int deci;
	
	printf ("Escribe un num en dec: "); scanf ("%d", &deci);
	printf ("El numero en hexadecimal es: %X\n", deci);
	
	return 0;
}
