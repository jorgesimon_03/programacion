#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	char cad_num[3];
	int num;
	
	printf ("Escribe un numero entre 32 y 128: ");
	scanf ("%d", &num);
	if(num>=32 && num<=128){
		printf ("El numero como entero es: %d\n", num);
		sprintf(cad_num, "%d", num);
		printf ("El numero como caracter es: %s\n", cad_num);
	}else
		printf ("El numero NO esta entre 32 y 128\n");

	return 0;
}
