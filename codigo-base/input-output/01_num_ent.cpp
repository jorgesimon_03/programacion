#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int num;

	printf ("Escribe un numero entero: ");
	scanf ("%d", &num);
	printf ("El numero es: %d\n", num);

	return 0;
}
