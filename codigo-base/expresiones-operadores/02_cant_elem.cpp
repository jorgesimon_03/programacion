#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int arr[]={12, 34, 56, 78, 90};

	int tmbt = sizeof(arr);	
	int tmel = sizeof(arr[0]);

	int media = tmbt/tmel;
	printf ("El array 'arr' tiene: %d elementos\n", media);

	return 0;
}
