#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int a;
	long int b;
	long long int c;

	printf ("La variable 'a' ocupa: %d\n", sizeof(a));
	printf ("La variable 'b' ocupa: %d\n", sizeof(b));
	printf ("La variable 'c' ocupa: %d\n\n", sizeof(c));

	float e;
	long double d;

	printf ("La variable 'd' ocupa: %d\n", sizeof(e));
        printf ("La variable 'e' ocupa: %d\n", sizeof(d));

	return 0;
}
