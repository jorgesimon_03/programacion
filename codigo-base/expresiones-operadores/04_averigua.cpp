#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int a=0;
	char b=6;

	while(b>0){
		b <<= 1;
		b |= (a = a++ % 2);
	}

	printf ("%d :: %s\n", a, b);

	return 0;
}
