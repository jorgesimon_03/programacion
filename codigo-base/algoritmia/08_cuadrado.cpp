#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void base(int num){
	for(int i=0; i<num; i++){
		printf ("*");
	}
	printf ("\n");
}

void pared(int num){
	int x = num-2;
	printf ("*");
	for(int j=0; j<x; j++){
		printf (" ");
	}
	printf ("*\n");
}

int main(){
	int lado;
	char espac[]=" ";

	printf ("lado??"); scanf ("%d", &lado);
	int pareds = lado-2;

	base(lado);
	for(int k=0; k<pareds; k++){
		pared(lado);
	}
	base(lado);

	return 0;
}
