#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int A[5][10];
	int num=9;
	for(int i=0; i<10; i++){
		A[0][i] = i;
	}
	for(int j=1; j<5; j++){
		for(int k=0; k<10; k++){
			A[j][k] = (A[j-1][k]+A[j-1][k+1]);
		}
	}
	for(int j=0; j<5; j++){
		for(int k=0; k<10; k++){
			if(k=9){
				A[j][k] = 9;
			}
		}
	}

	for(int j=0; j<5; j++){
		for(int k=0; k<10; k++){
			printf ("%d\t", A[j][k]);
		}
		printf ("\n");
	}

	return 0;
}
