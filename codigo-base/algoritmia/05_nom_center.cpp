#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

void borrar(){
	printf ("\e1;1H\e[2J");
}

void gotoxy(int x, int y){
	printf ("%c[%d;%df",0x1B,y,x);
}

int main(int argc, char **argv){
	struct winsize w;			//inicio de center
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	printf ("lines %d\n", w.ws_row);
	printf ("columns %d\n", w.ws_col);	//fin de center

	int x=w.ws_col/2;
	int y=w.ws_row/2;
	borrar();
	gotoxy(x, y);
	printf ("x\n");


	return 0;
}
