#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

void bases(int longi){
	for(int k=0; k<longi; k++){
		printf ("*");
	}
}

int main(){
	char nombre[MAX];
	
	printf ("Escribe tu nombre: "); scanf ("%s", nombre);
	int log = strlen(nombre)+4;

	bases(log);
	printf ("\n* %s *\n", nombre);
	bases(log);
	printf ("\n");

	return 0;
}
