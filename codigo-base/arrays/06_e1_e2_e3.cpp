#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(){
	int A[3][10];
	int j, k;
	int num=0, expo=1;
	int result;


	for(j=0; j<3; j++){
		for(k=0; k<10; k++){
			num += 3;
			result = pow(num, expo);
			A[j][k] = result;
		}
	}

	for(j=0; j<3; j++){
		for(k=0; k<10; k++){
			printf ("%d\t", A[j][k]);
		}
		printf ("\n");
	}
	
	return 0;
}
