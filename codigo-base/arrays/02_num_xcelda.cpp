#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int arr[10];
	arr[0]=0;
	
	printf ("arr[0]=%d\n", arr[0]);

	for(int i=1; i<10; i++){
		arr[i] = i+arr[i-1];
		printf ("arr[%d]=%d\n", i, arr[i]);
	}

	return 0;
}
