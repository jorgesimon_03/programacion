#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int A[10][10];
	int num=0;
	int j, k;

	for(j=0; j<10; j++){
		for(k=0; k<10; k++){
			A[j][k] = num;
		}
		num++;
	}

	for(j=0; j<10; j++){
		for(k=0; k<10; k++){
			printf ("%d ", A[j][k]);
		}
		printf ("\n");
	}

	return 0;
}
