#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(){
	char str1[]="s";
	char confirm[2];
	int res;

	printf ("Quieres descargar el archivo(s/n): "); scanf ("%s", confirm);
	res = strcmp(str1, confirm);

	if(res==0)
		printf ("Descargando...\n");
	else
		printf ("Descarga denegada\n");

	return 0;
}
