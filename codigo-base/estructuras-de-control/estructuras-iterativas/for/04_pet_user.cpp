#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

int main(){
	int num;
	char arr[MAX];

	printf ("Escribe el caracter que quieras q se repita: "); scanf ("%s", arr);
	printf ("Cuantas veces quieres q se repita: "); scanf ("%d", &num);

	for(int i=0; i<num; i++){
		printf ("%s ", arr);
	}
	printf ("\n");

	return 0;
}
