#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

int main(){
	char arr[MAX];

	scanf ("%s", arr);

	for(int i=0; i<10; i++){
		printf ("%s ", arr);
	}
	printf ("\n");

	return 0;
}
