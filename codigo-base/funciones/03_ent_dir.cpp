#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void pedir_entero(int *variable){
	printf ("Escribe un numero: ");
	scanf ("%d", variable);
}

int main(){
	int dir_var;

	pedir_entero(&dir_var);
	
	printf ("%d\n", dir_var);

	return 0;
}
