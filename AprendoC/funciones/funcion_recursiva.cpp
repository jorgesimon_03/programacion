#include <stdio.h>
#include <stdlib.h>

int factori(int num){
	if(num == 1){
		return (1);
	}
	else
		return (num * factori(num-1));
}

int main(){
	int x = 5;

	printf ("El factor de %i es %i \n", x, factori(x));

	return 0;
}
