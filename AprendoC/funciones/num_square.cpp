#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int square(){
	int num;
	printf ("Escriba un numero: ");
	scanf ("%i", &num);
	int numdob;
	numdob = num * num;
	printf ("El cuadrado de %i es %i \n", num, numdob);
	return 0;
}

int main(){
	printf ("Primer ejercicio \n");
	square();

	return 0;
}
