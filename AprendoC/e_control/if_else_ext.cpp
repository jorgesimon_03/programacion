#include <stdio.h>
#include <stdlib.h>

int es_multiplo_de (int valor, int multi){
	return valor % multi == 0;
}

int main(){

	int numero;

	printf ("Numero: ");
	scanf ("%i", &numero);

	if (numero == 3)
		printf ("Has puesto 3.\n");

	if (numero % 2 == 0)
		printf ("El numero %i es par \n", numero);
	else
		printf ("El numero %i es impar \n", numero);

	if (numero)
		printf ("El numero es diferente a 0 \n");

	if (!numero)
		printf ("%i es igual a 0 \n", numero);

	if (es_multiplo_de(numero, 3))
		printf ("%i es multiplo de 3.\n", numero);

	if (2<3 && 3<4)
		printf ("Verdadero \n");

	return 0;
}
