#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2){
	return op1 + op2;
}

int resta (int op1, int op2){
	return op1 - op2;
}

int preguntar_op (){
	static int nop = 0;
	int op;

	printf ("Operando %i: ", ++nop);
	scanf (" %i", &op);

	return op;
}

int main (){
	int op1, op2, result;

	op1 = preguntar_op ();
	op2 = preguntar_op ();

	result = suma (op1, op2);

	printf ("%i + %i = %i\n", op1, op2, result);

	return 0;
}
