#include <stdio.h>
#include <stdlib.h>

int main(){

	int op1, op2, result;

	printf ("Operando 1: ");
	scanf (" %i", &op1);

	printf ("Operando 2: ");
	scanf (" %i", &op2);

	result = op1 + op2;

	printf ("%i + %i = %i\n", op1, op2, result);

	printf ("\n\n");

	printf ("&%p: %i (%lu bytes)\n", &op1, op1, sizeof (op1));
	printf ("&%p: %i (%lu bytes)\n", &op2, op2, sizeof (op2));
	printf ("Las direcciones de memoria ocupan: %lu bytes.\n", sizeof (&result));

	return 0;
}
