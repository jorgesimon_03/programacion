#include <stdio.h>

int main(){

	int op1, op2, result;

	printf ("Operando 1: ");
	scanf (" %i", &op1);

	printf ("Operando 2: ");
	scanf (" %i", &op2);

	result = op1 + op2;

	printf ("%i + %i = %i\n", op1, op2, result);

	return 0;
}
