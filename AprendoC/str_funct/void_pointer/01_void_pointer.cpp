#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

int main(){
	int x = 12;
	float y = 10.75;
	char dile = 'f';
	void *ptr;

	ptr = &x;
	printf ("void ptr apunta a: %d\n", *((int *)ptr));
	ptr = &y;
	printf ("void ptr apunta a: %f\n", *((float *)ptr));
	ptr = &dile;
	printf ("void ptr apunta a: %c\n", *((char *)ptr));

	return 0;
}
