#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void* square(const void *num){
	static int result;
	result = (*(int *)num) * (*(int *)num);
	return &result;
}

int main(){
	int x, *sq_int;
	x = 6;
	sq_int = (int *)square(&x);
	printf ("%d squared is %d\n", x, *sq_int);

	return 0;
}
