#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
	char info[]="ArturoSoria WA 28033";
	char city[50];
	char state[50];
	int poblacion;

	sscanf(info, "%s %s %d", city, state, &poblacion);
	printf("%d viven en %s, %s\n", poblacion, city, state);

	return 0;
}
