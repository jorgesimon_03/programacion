#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void say_hello(int num_time){
	int k;
	for(k=0; k<num_time; k++){
		printf ("Hello\n");
	}
}

int main(){
	void (*funptr)(int);		// puntero de funcion
	funptr = say_hello;		// asignacion de puntero
	funptr(3);			// llamada de funcion
	
	return 0;
}
