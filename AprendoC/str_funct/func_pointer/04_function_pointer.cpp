#include <stdio.h>
#include <stdlib.h>

int main(){
	int j = 63;
	int *p = NULL;
	p = &j;

	printf ("La direccio de j es: %x\n", &j);
	printf ("p contiene direccion %x\n", p);
	printf ("El valor de j es: %d\n", j);
	printf ("p apunta al valor %d\n", *p);

	return 0;
}
