#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sum(int x, int y){
	return x+y;
}

int rest(int x, int y){
	return x-y;
}

int mult(int x, int y){
	return x*y;
}

int divi(int x, int y){
	if(y!=0)
		return x/y;
	else
		return 0;
}

int main(){
	int x, y, choice, result;
	
	int (*op[4])(int, int);
	op[0]=sum;
	op[1]=rest;
	op[2]=mult;
	op[3]=divi;
	
	printf ("Introduce dos enteros: ");
	scanf ("%d %d", &x, &y);

	printf ("0 para sumar, 1 para restar, 2 para multiplicar, 3 para dividir: ");
	scanf ("%d", &choice);

	result = op[choice](x, y);
	printf ("El resultado es: %d\n", result);

	return 0;
}
