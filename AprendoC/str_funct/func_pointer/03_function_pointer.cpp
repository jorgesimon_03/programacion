#include <stdio.h>
#include <stdlib.h>

void test(int k){
	printf ("eL puntero de k %x \n", &k);
}

int main(){
	int i = 0;

	printf ("el puntero of i %x \n", &i);
	test(i);
	printf ("El puntero of i %x \n", &i);
	test(i);

	return 0;
}
